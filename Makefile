STOG=./_build/default/mystog.exe
STOG_SERVER=./_build/default/mystog_server.exe
STOG_OCAML_SESSION=./_build/default/mystog_ocaml_session.bc.exe
DEST_DIR=/tmp/formation-ocaml
BASE_URL_OPTION=
ROOT:=`pwd`
OCAML_SESSION=$(ROOT)/$(STOG_OCAML_SESSION) -w -3 `echo \`ocamlfind query -i-format inspect parmap lwt lwt.unix\` -ppx "/home/guesdon/.opam/5.2.0/lib/lwt_ppx/ppx.exe\\ -as-ppx"`
STOG_OPTIONS=--stog-ocaml-session "$(OCAML_SESSION)" --default-lang fr --verbose-level debug -d $(DEST_DIR) $(BASE_URL_OPTION)
MORE_OPTIONS=
LESSC=lessc
CP=cp -f

EXERCICES=

build:
	dune build
	$(MAKE) site

style.css: less/*.less
	$(LESSC) less/style.less > $@

site: style.css
	$(STOG) $(STOG_OPTIONS) $(MORE_OPTIONS) .
	$(CP) -f slide_arbre*.png $(DEST_DIR)/slides/
	$(CP) style.css $(DEST_DIR)/style.css

test:
	$(MAKE) BASE_URL_OPTION="--site-url file://$(DEST_DIR)" build


server:
	$(STOG_SERVER) $(STOG_OPTIONS) .

clean:
	dune clean
	rm -f $(BLOG_EXAMPLES)
	rm -f *.cm? posts/*.cm? codes/*.cm? *.cmxs codes/*/*.cm*
	rm -f *.o posts/*.o codes/*.o codes/*/*.o
	(cd codes && rm -f \
		imperative/myecho \
		stdlib/myprintenv \
		stdlib/lstmp \
		para/lwt-commands \
		para/lwt-grep \
		para/thread-print \
		para/file*.txt \
		fstclassmod/exemple \
		progmod/mon_programme)
	rm -fr .stog/cache

nocache:
	$(MAKE) MORE_OPTIONS=--nocache test

.SUFFIXES: .ml .cmo

%.cmo: %.ml
	ocamlfind ocamlc -package threads,stog -thread -rectypes -c $<

%.cmxs: %.ml
	ocamlfind ocamlopt -shared -package stog -rectypes -o $@  $<

%.cmx: %.ml
	ocamlfind ocamlopt -c -package stog -rectypes  $<


blog_examples: $(BLOG_EXAMPLES)
BLOG_EXAMPLES= \
	posts/date_du_jour \
	posts/code_morse

posts/date_du_jour: posts/date_du_jour.ml
	ocamlopt -o $@ unix.cmxa $^

posts/code_morse: posts/code_morse.ml
	ocamlopt -o $@ $^

